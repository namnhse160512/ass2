﻿using Ass2.DAL;
using Ass2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ass2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchController : ControllerBase
    {
        private readonly MyAppDbContext _appDbContext;

        public BranchController(MyAppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var branches = _appDbContext.Branches.ToList();
                if (branches.Count == 0)
                {
                    return NotFound("Branches not avaliable.");
                }
                return Ok(branches);
            }
            catch (Exception ex)
            {

                return BadRequest(ex);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id) 
        {
            try
            {
                var branches = _appDbContext.Branches.Find(id);
                if (branches == null)
                {
                    return NotFound($"Branches details not found with id {id}");
                }
                return Ok(branches);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Post(Branches model)
        {
            try
            {
                _appDbContext.Add(model);
                _appDbContext.SaveChanges();
                return Ok("Branches created.");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Put(Branches model)
        {
            if(model == null || model.BranchId == 0)
            {
                if(model == null) {
                    return BadRequest("Model data is valid.");
                }  else if(model.BranchId == 0)
                {
                    return BadRequest($"Branches Id {model} is invalid");
                }
            }
            var branches = _appDbContext.Branches.Find(model.BranchId);
            if(branches == null)
            {
                return NotFound($"Branches not found with id {model.BranchId}");
            }
            try
            {
                branches.Name = model.Name;
                branches.Address = model.Address;
                branches.City = model.City;
                branches.State = model.State;
                branches.Zipcode = model.Zipcode;
                _appDbContext.SaveChanges();
                return Ok("Branches details updated.");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            try
            {
                var branches = _appDbContext.Branches.Find(id);
                if (branches == null)
                {
                    return NotFound($"Branches not found with id {id}");
                }
                _appDbContext.Branches.Remove(branches);
                _appDbContext.SaveChanges();
                return Ok("Branches details deleted.");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
    }
}
