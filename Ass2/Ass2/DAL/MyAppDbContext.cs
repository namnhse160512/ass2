﻿using Ass2.Models;
using Microsoft.EntityFrameworkCore;

namespace Ass2.DAL
{
    public class MyAppDbContext : DbContext
    {
        public MyAppDbContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Branches> Branches { get; set; }
    }
}
