﻿using System.ComponentModel.DataAnnotations;

namespace Ass2.Models
{
    public class Branches
    {
        [Key] public int BranchId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string State { get; set; }
        [Required]
        public string Zipcode { get; set; }
    }
}
